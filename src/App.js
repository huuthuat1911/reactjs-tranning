import React from 'react';
import Footer from './Components/Layout/Footer/Footer'
import Routers from '../src/Components/Router/Router';
import {
  BrowserRouter as Router,
  Route,
  Link
} from "react-router-dom";


function App() {
  return (
     <Router>
        <div className="App">
          <Routers />
          <Footer />
        </div>
     </Router>
  );
}

export default App;
