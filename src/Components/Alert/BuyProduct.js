import React, { Component } from 'react';

class BuyProduct extends Component {
    constructor(props) {
        super(props);
        this.state= {
            isBuyStatus : false
        }
    }
   
 
    closeAlert = () => {
        this.props.callBackClose()
    } 

    render() {
        
        
        return (
            <div>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Thêm Thành Công !</strong>
                <button type="button" class="close" onClick={this.closeAlert} >
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
            </div>
        );
    }
}

export default BuyProduct;