import React, { Component } from 'react';
import HomeIcon from '../../../Images/home.svg';
import Food from '../../../Images/food.svg';
import Sale from '../../../Images/sale.svg';
import Phone from '../../../Images/phone.svg';
import {
    BrowserRouter as Router,
    Route,
    NavLink,
    NavNavLink 
} from "react-router-dom";


class Footer extends Component {
    render() {
        return (
            <div>
                <ul className='list-footer'>
                    <li>
                        <NavLink to="/homepage"> <img src={HomeIcon} />Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/productpage"><img src={Food} />Product</NavLink>
                    </li>
                    <li>
                        <NavLink to="/salepage"><img src={Sale} />Sale</NavLink>
                    </li>
                    <li>
                        <NavLink to="/aboutpage"><img src={Phone} />About</NavLink>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Footer;