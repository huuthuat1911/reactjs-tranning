import React, { Component } from 'react';
import Background from '../../../Images/bg-masthead.jpg'

var bgbanner = {
    backgroundImage: `url(${Background})`,
    width: "100%",
}


class Banner extends Component {
    render() {
        return (
            <div>
                <header className="masthead" style={bgbanner}>
                    <div className="container h-100">
                        <div className="row h-100 align-items-center justify-content-center text-center">
                            <div className="col-lg-10 align-self-end">
                                <h1 className="text-uppercase text-white font-weight-bold">Enabling the coffee sustainability journey</h1>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

export default Banner;