import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from "react-router-dom";
import Home from '../../Components/Page/Home/Home';
import Product from '../../Components/Page/Product/Product';
import About from '../Page/About/About';
import Sale from '../Page/Sale/Sale';
import ListProduct from '../Page/Product/ListProduct';

class Routers extends Component {
    render() {
        return (

                <div>
                    <Route exact path="/homepage" component={Home} />
                    <Route path="/productpage" component={Product} />
                    <Route path="/salepage" component={Sale} />
                    <Route path="/aboutpage" component={About} />
                    <Route path="/listproduct" component={ListProduct} />
                </div>

        );
    }
}

export default Routers;