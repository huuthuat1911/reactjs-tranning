import React, { Component } from 'react';
import edit from '../../../Images/edit.svg'

class About extends Component {
    render() {
        return (
            <div className='about'>
                <div className='top-about'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col-4'>
                                <div className='thumnail'>
                                    <img src="https://ss-images.catscdn.vn/wpm450/2020/04/14/7341405/midu-4.jpg" class="img-fluid" alt="" />
                                </div>
                            </div>
                            <div className='col-8'>
                                <h4 className='text-left'>Lê Minh Trang</h4>
                                <p className='text-left mb-1'>
                                    Gmail: <strong>minhtrang@gmail.com</strong>
                                </p>
                                <p className='text-left'>
                                    Điện Thoại: <strong>0978787878</strong>
                                </p>
                            </div>
                        </div>
                    </div>
                    <button type="button" className="btn btn-edit" data-toggle="modal" data-target="#exampleModal">
                        <img src={edit} />
                    </button>
                </div>
                <div className='tiem-content-about'>
                    <div className='container'>
                        <p className='text-left'>
                            Full Name: <strong>Lê Minh Trang</strong>
                        </p>
                        <p className='text-left'>
                            Địa chỉ: <strong>234 Tiểu La, Đà Nẵng</strong>
                        </p>
                        <p className='text-left'>
                            Giỏ Hàng: <strong>3</strong>
                        </p>
                    </div>
                </div>
                <div className="modal fade" id="exampleModal" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="form-group">
                                        <label className="text-left d-block">UserName</label>
                                        <input type="email" className="form-control" aria-describedby="emailHelp" />
                                    </div>
                                    <div className="form-group">
                                        <label className="text-left d-block">Email</label>
                                        <input type="email" className="form-control" aria-describedby="emailHelp" />
                                    </div>
                                    <div className="form-group">
                                        <label className="text-left d-block">Địa Chỉ</label>
                                        <input type="email" className="form-control" aria-describedby="emailHelp" />
                                    </div>
                                    <div className="form-group">
                                        <label className="text-left d-block">Password</label>
                                        <input type="email" className="form-control" aria-describedby="emailHelp" />
                                    </div>
                                    <div className="form-group">
                                        <label className="text-left d-block">Confirm Password</label>
                                        <input type="email" className="form-control" aria-describedby="emailHelp" />
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default About;