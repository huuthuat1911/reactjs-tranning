import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
  } from "react-router-dom";

class CardItem extends Component {
    
    render() {
        console.log(this.props.name);
        return (

            <div className="col-6 mt-3">
                <div className="card h-100">
                    <img src={this.props.img} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{this.props.name}</h5>
                        <p className="card-text">{this.props.content}</p>
                    </div>
                    <div className="link-buy">
                            <Link to='/listproduct'>
                                Xem Thêm
                            </Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default CardItem;