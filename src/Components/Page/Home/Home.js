import React, { Component } from 'react';
import CardItem from './CardItem';
import Banner from '../../Layout/Banner/Banner'
import data from '../../../Data/data.json'

class Home extends Component {
    componentDidMount(){
        localStorage.setItem('bgcolor', 'red');
      }
    
    render() {
        return (
            <div>
                <Banner />
                <div className="container">
                <div className=" row list-card">

                    {
                        data.Product.map((value, key) => {
                            return (
                                <CardItem name={value.name} img={value.img} content={value.content}>
                                </CardItem>
                            )
                        })
                    }

                </div>
            </div>
            </div>
        );
    }
}

export default Home;