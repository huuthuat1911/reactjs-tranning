import React, { Component } from 'react';
import ItemProduct from '../Product/ItemProduct';
import data from '../../../Data/data.json'

class Sale extends Component {
    constructor(props) {
        super(props);
    }
    render() {

        let sale = 
            data.list.ListCoffe.filter(status => status.sale == true).map((value, key) => {
                return (
                 <ItemProduct name={value.name} img={value.img} prices={value.prices} status={value.status} sale={value.sale} salenumber={value.salenumber}>
                 </ItemProduct>
            )
        });

        return (
            <div>
               <div className='container'>
                <div className='row'>
                {
                      sale      
                        }
                </div>
               </div>
            </div>
        );
    }
}

export default Sale;