import React, { Component } from 'react';
import Banner from '../../Layout/Banner/Banner';
import data from '../../../Data/data.json';
import ItemProduct from './ItemProduct'

class ListProduct extends Component {

    constructor(props) {
        super(props);
        this.state = {
            keyword: '',
            products: []
        }
    }

    onChange = (e) => {
        let keyword = e.target.value;
        let products = data.list.filter(dt => dt.name.toLowerCase().includes(keyword.toLowerCase()));
        this.setState({
            keyword, products
        })
    }

    componentDidMount() {
        this.fetchProducts();
    }

    fetchProducts = () => {
        this.setState({
            products: data.list.ListCoffe
        })
    }

    render() {
        return (
            <div>
               <Banner />
                <div className="container">
                    <div className="panel panel-success mt-3">
                        <div className="panel-body">
                            <form className="form-inline my-2 my-lg-0 d-flex">
                                <input className="form-control  mr-sm-2" type="text" placeholder="Search" aria-label="Search" value={this.state.keyword} onChange={this.onChange} />
                            </form>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className=" row list-card">
                        {
                            this.state.products.map((value, key) => {
                                let product = '';
                                    product =  <ItemProduct name={value.name} img={value.img} prices={value.prices} status={value.status} sale={value.sale} salenumber={value.salenumber}>
                                    </ItemProduct>
                                return product;
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default ListProduct;