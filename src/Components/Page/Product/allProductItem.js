


import React, { Component, Fragment } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from "react-router-dom";
import BuyProduct from '../../Alert/BuyProduct';

class allProductItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isBuyStatus: false,
        }
    }

    addToCard = () => {

        this.setState({
            isBuyStatus: true
        })
    }


    closeFuction = () => {
        this.setState({
            isBuyStatus: false
        })
    }

    render() {
        console.log(this.props, 'props');
        
        let status;
        if (this.props.status) {
            status = (
                <button type="button" className="btn btn-buy text-white btn-danger w-100" onClick={this.addToCard}>
                    Thêm vào giỏ hàng
                </button>
            )
        }

        let statussale;
        if (this.props.sale) {
            statussale = (
                <p className="sale-number">
                            {this.props.salenumber} <span>%</span>
                        </p>
            )
        }


        return (

            <div className="col-6 mt-3">
                <div className="alert-status">
                    {this.state.isBuyStatus === true ? <BuyProduct callBackClose={this.closeFuction} /> : ''}
                </div>
                <div className="card h-100">
                    <img src={this.props.img} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{this.props.name}</h5>
                        <p className="card-text">{this.props.prices} <span>đ</span></p>
                    </div>
                    <strong className="mb-1">
                        {this.props.status ? 'Còn Hàng' : 'hết hàng'}
                    </strong>
                    <div>
                    {this.props.status ? statussale : ''}
                    </div>
                    <div className="link-buy">
                        {status}
                    </div>

                </div>
            </div>
        );
    }
}

export default allProductItem;