import React, { Component } from 'react';
import Banner from '../../Layout/Banner/Banner';
import data from '../../../Data/data.json';
import ItemProduct from './allProductItem'

class Product extends Component {

    constructor(props) {
        super(props);
        this.state = {
            keyword: '',
            products: [],
            sort : {
                by: 'name',
                value: 1
            }
        }
    }

    onChange = (e) => {
        let keyword = e.target.value;
        let products = data.list.ListCoffe.filter(dt => dt.name.toLowerCase().includes(keyword.toLowerCase()));
        products = [...products, ...data.list.ListDrink.filter(dt => dt.name.toLowerCase().includes(keyword.toLowerCase()))];
        this.setState({
            keyword, products
        })
    }

    componentDidMount() {
        this.fetchProducts();
    }

    fetchProducts = () => {
        this.setState({
            products: [...data.list.ListCoffe, ...data.list.ListDrink],
        })
    }

    onClick =(sortBy) => {
        this.setState({
            sort: {
                by: sortBy
            }
        })
        console.log(this.state)
    }

    render() {
     
        return (
            <div>
                <Banner />
                <div className="container">
                    <div className="panel panel-success mt-3">
                        <div className="panel-body">
                            <form className="form-inline my-2 my-lg-0 d-flex">
                                <input className="form-control  mr-sm-2" type="text" placeholder="Search" aria-label="Search" value={this.state.keyword} onChange={this.onChange} />
                            </form>
                        </div>
                    </div>
                </div>

                <div className="dropdown">
                    <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        A-Z
                    </button>
                    <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li className="dropdown-item" onClick={ () => this.onClick('name', 1) }>
                            <a role="button" className={
                                this.state.by === 'name' ? 'sortactive': ''
                            }>
                            A-Z
                            </a>
                        </li>
                        <li className="dropdown-item" onClick={ () => this.onClick('name', -1) }>
                            <a role="button">
                            Z-A
                            </a>
                        </li>
                    </ul>
                </div>

                <div className="container">
                    <div className=" row list-card">
                        {
                            this.state.products.map((value, key) => {
                                let product = '';
                                product = <ItemProduct name={value.name} img={value.img} prices={value.prices} status={value.status} sale={value.sale} salenumber={value.salenumber}>
                                </ItemProduct>
                                return product;
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default Product;